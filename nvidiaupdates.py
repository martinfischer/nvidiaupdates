"""
nvidiaupdates.py

Sucht nach neuen Updates für meinen Linux Nvidia Grafikkartentreiber
"""
import os
import requests
from datetime import datetime
from bs4 import BeautifulSoup


def write_log(message):
    """
    Schreibt ein Logfile
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    programm_filename = os.path.basename(__file__)
    message = "{:20}-->  {}  -->  {}\n".format(programm_filename, timestamp, message)
    logfile = os.path.expanduser("~/.applog")
    with open(logfile, "a") as f:
        f.write(message)


def send_message(title, message, sound, priority):
    """
    Sendet eine Pushover Notifikation an mein Tab
    """
    APP_TOKEN = "APP_TOKEN"
    USER_KEY = "USER_KEY"
    payload = {
        "token": APP_TOKEN,
        "user": USER_KEY,
        "title": title,
        "message": message,
        "sound": sound,
        "priority": priority
    }
    try:
        r = requests.post("https://api.pushover.net/1/messages.json",
                          data=payload)
        return r
    except:
        return False


def read_update_dates(update_dates_file):
    """
    Liest vorhandene Update Daten aus einer Datei
    """
    with open(update_dates_file) as f:
        update_dates = f.read().splitlines()
    return update_dates


def write_update_date(update_dates_file, update_date):
    """
    Schreibt neue Update Daten in eine Datei
    """
    with open(update_dates_file, "a") as f:
        f.write("{}\n".format(update_date))


def get_latest_update(url):
    """
    Holt das neueste Update Datum
    """
    try:
        r = requests.get(url)
        html = r.text
        soup = BeautifulSoup(html, "html.parser")
        text = soup.get_text()
        lastversiontext = "Letzte langlebige Branch-Version: "
        shorter_text = text[text.find(lastversiontext) + len(lastversiontext):]
        end = "\r"
        latest_version = shorter_text[:shorter_text.find(end)]
        return (latest_version,)
    except Exception as error:
        return ("Error", error)


def check_update(url, update_dates_file, sound, priority):
    """
    Die Hauptfunktion, die alles einleitet
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    latest_update_tuple = get_latest_update(url)
    latest_update = latest_update_tuple[0]
    if latest_update == "Error":
        result = send_message("Nvidiaupdates Error", "Error: {}".format(
                                        latest_update_tuple[1]), "updown", "0")
        write_log("{} ERROR: {} Pushover response: {}".format(
                          "nvidia", latest_update_tuple[1], result))
    else:
        if latest_update in read_update_dates(update_dates_file):
            write_log("No new nvidia driver found")
        else:
            title = "New nvidia driver {}".format(latest_update)
            message = "New nvidia driver {} is out! Check: {}".format(
                                                            latest_update, url)
            result = send_message(title, message, sound, priority)
            if result:
                write_log(("New nvidia driver {} found! "
                          "Pushover response: {}").format(latest_update, str(result)))
            else:
                write_log(("New nvidia driver {} found! "
                          "Pushover connection error").format(latest_update))
            write_update_date(update_dates_file, latest_update)


if __name__ == "__main__":
    nvidia_url = "http://www.nvidia.de/object/unix-de.html"
    nvidia_update_dates_file = "update_dates_nvidia.txt"
    check_update(nvidia_url, nvidia_update_dates_file, "echo", "1")
