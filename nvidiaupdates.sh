#! /bin/bash
cd ~/pyvenvs/python3/nvidiaupdates
source bin/activate

cd ~/workspace/nvidiaupdates

# virtualenv is now active, which means your PATH has been modified and python
# can be called like this.

python nvidiaupdates.py

deactivate

# a sample crontab entry:
# to check for jobs by the loged in user: crontab -l
# to edit/create the jobs for the loged in user: crontab -e
# entry: 15 7 * * * /home/martin/workspace/nvidiaupdates/nvidiaupdates/nvidiaupdates.sh
